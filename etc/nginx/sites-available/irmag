# ft:nginx

geo $im_internal_client {
    default no;
    127.0.0.1 yes;
    192.168.0.0/16 yes;
}

# HTTP to HTTPS
server {
    listen 80 default_server;
    server_name irmag.local www.irmag.local profile.irmag.local admin.irmag.local blog.irmag.local forum.irmag.local;

    return 301 https://$host$request_uri;
}

# HTTP m.irmag.local to HTTPS irmag.local
server {
    listen 80;
    server_name m.irmag.local;

    return 301 https://irmag.local$request_uri;
}

# HTTPS m.irmag.local to HTTPS irmag.local
server {
    listen 443 ssl http2;
    server_name m.irmag.local;

    include conf/ssl.conf;

    return 301 https://irmag.local$request_uri;
}

# HTTPS default
server {
    listen 443 ssl http2 deferred;
    server_name irmag.local profile.irmag.local admin.irmag.local blog.irmag.local forum.irmag.local;

    include conf/ssl.conf;
    include conf/symfony.conf;

    error_log /var/log/nginx/irmag_error.log;
    access_log /var/log/nginx/irmag_access.log combined buffer=16k;
}

# HTTPS www to HTTPS non www
server {
    listen 443 ssl http2;
    server_name www.irmag.local;

    include conf/ssl.conf;

    return 301 https://irmag.local$request_uri;
}

# HTTPS обмен с 1С (for local only)
server {
    listen 443 ssl http2;
    server_name exchange-1c.irmag.local;

    # только для локального использования
    if ($im_internal_client = no) {
        return 403;
    }

    #fastcgi_send_timeout 86400;
    fastcgi_read_timeout 86400; # сутки

    include conf/ssl.conf;
    include conf/symfony.conf;

    error_log /var/log/nginx/irmag_exchange_1c_error.log;
    access_log /var/log/nginx/irmag_exchange_1c_access.log combined buffer=16k;
}

# HTTPS API (for local only)
server {
    listen 443 ssl http2;
    server_name api.irmag.local;

    # только для локального использования
    if ($im_internal_client = no) {
        return 403;
    }

    #fastcgi_send_timeout 86400;
    fastcgi_read_timeout 86400; # сутки

    include conf/ssl.conf;
    include conf/symfony.conf;

    #location ~ ^/userbonushistories {
    #    return 503;
    #}

    error_log /var/log/nginx/irmag_api_error.log;
    access_log /var/log/nginx/irmag_api_access.log combined buffer=16k;
}
