# Пользователь и группа, от имени которых будет запущен процесс
user www-data www-data;
# Число воркеров в новых версиях рекомендовано устанавливать в auto
worker_processes auto;
#worker_processes 4;
# Директива задаёт приоритет рабочих процессов от -20 до 20
# (отрицательное число означает более высокий приоритет).
worker_priority -15;
# Уменьшает число системных вызовов gettimeofday(),
# что приводит к увеличению производительности.
timer_resolution 100ms;
# Изменяет ограничение на число используемых файлов RLIMIT_NOFILE
# для рабочего процесса (ulimit -n 65536).
worker_rlimit_nofile 20000;

events {
    # Максимальное количество соединений одного воркера
    worker_connections 2048;
    # Метод выбора соединений
    use epoll;
    # Принимать максимально возможное количество соединений
    multi_accept on;
}

http {
    # Устанавливаем лимит на 500 соединений с одного ip
    limit_conn_zone $binary_remote_addr zone=perip:10m;
    limit_conn perip 500;

    # Указываем файл с mime-типами и указываем тип данных по-умолчанию
    include mime.types;
    default_type application/octet-stream;

    # кодировка по-умолчанию
    charset utf-8;
    # Метод отправки данных sendfile эффективнее чем read+write
    sendfile on;
    # Ограничивает объём данных, который может передан за один вызов sendfile(). Нужно для исключения ситуации когда одно соединение может целиком захватить воркер
    sendfile_max_chunk 128k;
    # Отправлять заголовки и и начало файла в одном пакете
    tcp_nopush on;
    tcp_nodelay on;
    # Сбрасывать соединение если клиент перестал читать ответ
    reset_timedout_connection on;
    # Устанавливать Keep-Alive соединения с посетителями
    keepalive_timeout 60;
    # Задаём максимальный размер хэш-таблиц типов
    types_hash_max_size 2048;
    # Разрывать соединение по истечению таймаута при получении заголовка и тела запроса
    client_header_timeout 3;
    client_body_timeout 5;
    # Разрывать соединение, если клиент не отвечает в течение 3 секунд
    send_timeout 3;
    # Отключить вывод версии nginx в ответе
    server_tokens off;
    # Задание буфера для заголовка и тела запроса
    client_header_buffer_size 2k;
    client_body_buffer_size 256k;
    # Ограничение на размер тела запроса
    client_max_body_size 24m;

    # timeouts
    proxy_send_timeout 120;
    proxy_read_timeout 120;
    fastcgi_send_timeout 120;
    fastcgi_read_timeout 120;

    # server_names_hash_bucket_size 64;
    # server_name_in_redirect off;

    # Включаем зжатие
    gzip on;
    # Устанавливает минимальную длину ответов, для которых будет применяться сжатие.
    # По умолчанию 20, но имеет смысл поставить больше, так как постоянное сжатие приведет
    # к повышению нагрузки на процессоры сервера и клиента.
    gzip_min_length 1000;
    # 1 (худшее) - 9 (лучшее)
    gzip_comp_level 5;
    # gzip mime types
    gzip_types text/plain text/xml text/css text/javascript text/csv image/svg+xml application/x-javascript application/xml application/xml+rss application/ecmascript application/json application/javascript application/pdf application/postscript;

    # Кеширование дескрипторов файлов
    open_file_cache          max=20000 inactive=20s;
    open_file_cache_valid    120s;
    open_file_cache_min_uses 5;
    open_file_cache_errors   off;

    # Логи
    access_log /var/log/nginx/access.log combined buffer=16k;
    error_log /var/log/nginx/error.log;

    # Сайты
    include /etc/nginx/sites-enabled/*;
}
