# IRMAG server configs

## Nginx with http2 support

minimum stability: 1.6

## Varnish

minimum stability: 4.0

## Sphinx

sphinxsearch minimum stability: >=2.2.*

### Install

```bash
apt-get install sphinxsearch
```

### Config

```bash
cd irmag-server-configs
sudo ln -s $PWD/etc/sphinxsearch/sphinx.conf /etc/sphinxsearch/
sudo ln -s $PWD/var/lib/sphinxsearch/data/stopwords.txt /var/lib/sphinxsearch/data/
sudo ln -s $PWD/var/lib/sphinxsearch/data/wordforms.txt /var/lib/sphinxsearch/data/
```

### Use

#### Manual

```code
indexer --rotate --all
```

#### Cron

every 5 min reindex

```bash
*/5 * * * * /usr/bin/indexer --rotate --all > /dev/null 2>&1
```
